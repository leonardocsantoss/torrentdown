# -*- coding: utf-8 -*-
# Load settings first
try:
    from settings import *
except ImportError:
    pass

# Now override any of them
LOCAL = False
DEBUG = False
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'torrentdown',
        'USER': 'torrentdown',
        'PASSWORD': '$iellos@torrentdown$',
        'HOST': '',
        'PORT': '',
    }
}

ADMIN_MEDIA_PREFIX = '/static/admin/'
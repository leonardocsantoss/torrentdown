# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Download.download_rate'
        db.alter_column('down_download', 'download_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2))

        # Changing field 'Download.upload_rate'
        db.alter_column('down_download', 'upload_rate', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=2))

    def backwards(self, orm):

        # Changing field 'Download.download_rate'
        db.alter_column('down_download', 'download_rate', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2))

        # Changing field 'Download.upload_rate'
        db.alter_column('down_download', 'upload_rate', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=2))

    models = {
        'down.download': {
            'Meta': {'object_name': 'Download'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'download_rate': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'magnetic': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'default': "'0'"}),
            'torrent': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'upload_rate': ('django.db.models.fields.DecimalField', [], {'default': "'0'", 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'})
        }
    }

    complete_apps = ['down']
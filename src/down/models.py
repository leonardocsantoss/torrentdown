# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from datetime import datetime
from django.db.models import signals

import libtorrent as lt
import time
from threading import Thread

import os
import shutil



class Download(models.Model):

    class Meta:
        verbose_name = u"Download"
        verbose_name_plural = u"Downloads"

    description = models.CharField(u"Descrição", max_length=128)
    name = models.CharField(max_length=1024, blank=True, null=True)
    magnetic = models.CharField(u"Magnetic link", max_length=1024, blank=True, null=True)
    torrent = models.FileField(u"Torrent", upload_to="torrents", blank=True, null=True)
    date = models.DateTimeField(u"Data de criação", default=datetime.now)
    status = models.PositiveIntegerField(u"Status %", default='0')
    download_rate = models.FloatField(u"Download (Kb/s)", blank=True, default="0")
    upload_rate = models.FloatField(u"Upload (Kb/s)", blank=True, default="0")

    def path_root(self):
        return '%s' % os.path.join(settings.MEDIA_ROOT, 'downloads')

    def path_file_root(self):
        return '%s' % os.path.join(settings.MEDIA_ROOT, 'downloads', '%s' % self.name)

    def path_file_url(self):
        return '%s' % os.path.join(settings.MEDIA_URL, 'downloads', '%s' % self.name)

    def _download_magnetic(self):

        def __download_magnetic(instance):
            ses = lt.session()
            ses.listen_on(6881, 6891)
            handle = lt.add_magnet_uri(
                ses,
                str(instance.magnetic), {
                 'save_path': instance.path_root(),
                 'duplicate_is_error': True,
                 'storage_mode': lt.storage_mode_t(2),
                 'paused': False,
                 'auto_managed': True,
                }
            )
            #Download metadata
            while (not handle.has_metadata()): time.sleep(10)
            instance.name = handle.name()
            instance.save()

            handle.set_upload_limit(524288)
            #Download file
            while True:
                status = handle.status()
                if instance.status != status.progress * 100 or instance.download_rate != status.download_rate / 1000 or instance.upload_rate != status.upload_rate / 1000:
                    instance.status =  status.progress * 100
                    instance.download_rate = status.download_rate / 1000
                    instance.upload_rate = status.upload_rate / 1000
                    instance.save()
                time.sleep(10)

        instance = self
        th = Thread(target=__download_magnetic, args=(instance, ))
        th.start()

    def _download_torrent(self):
        
        def __download_torrent(instance):
            ses = lt.session()
            ses.listen_on(6881, 6891)
            info = lt.torrent_info(instance.torrent.path)
            handle = ses.add_torrent({'ti': info, 'save_path': instance.path_root()})
            instance.name = handle.name()
            instance.save()

            handle.set_upload_limit(524288)
            #Download file
            while True:
                status = handle.status()
                if instance.status != status.progress * 100 or instance.download_rate != status.download_rate / 1000 or instance.upload_rate != status.upload_rate / 1000:
                    instance.status =  status.progress * 100
                    instance.download_rate = status.download_rate / 1000
                    instance.upload_rate = status.upload_rate / 1000
                    instance.save()
                time.sleep(10)

        instance = self
        th = Thread(target=__download_torrent, args=(instance, ))
        th.start()

    def download(self):
        if self.magnetic:
            self._download_magnetic()
        elif self.torrent:
            self._download_torrent()

    def _actions(self):
        acoes = u"<div style=\"min-width: 250px\">"
        acoes += u"<a style=\"padding-left: 7px;\" href=\"%s\"><img src=\"%sadmin/img/admin/icon_changelink.gif\" />%s</a>" % (self.pk, settings.STATIC_URL, u"Editar")
        acoes += u"<a style=\"padding-left: 7px;\" href=\"javascript://\" onClick=\"(function($) { $('input:checkbox[name=_selected_action]').attr('checked', ''); $('input:checkbox[name=_selected_action][value=%s]').attr('checked', 'checked'); $('select[name=action]').attr('value', 'start_torrent'); $('#changelist-form').submit(); })(jQuery);\" ><img src=\"%sadmin/img/admin/arrow_refresh.png\" />%s</a>" % (self.pk, settings.STATIC_URL, u"Start torrent")
        acoes += u"<a style=\"padding-left: 7px;\" href=\"javascript://\" onClick=\"(function($) { $('input:checkbox[name=_selected_action]').attr('checked', ''); $('input:checkbox[name=_selected_action][value=%s]').attr('checked', 'checked'); $('select[name=action]').attr('value', 'delete_selected'); $('#changelist-form').submit(); })(jQuery);\" ><img src=\"%sadmin/img/admin/icon_deletelink.gif\" />%s</a>" % (self.pk, settings.STATIC_URL, u"Remover")
        if self.status >= 100:
            acoes += u"<a style=\"padding-left: 7px;\" href=\"%s\" target=\"_blank\"><img src=\"%sadmin/img/admin/arrow_down.png\" />%s</a>" % (self.path_file_url(), settings.STATIC_URL, u"Download")
        acoes += "</div>"
        return acoes
    _actions.allow_tags = True
    _actions.short_description = u"Ações"

    def __unicode__(self):
        return self.description

def start_download(sender, instance, created, raw, using, **kwargs):
    if created:
        instance.download()
signals.post_save.connect(start_download, sender=Download)
def delete_download(sender, instance, using, **kwargs):
    try:
        if instance.torrent:
            instance.torrent.delete()
        shutil.rmtree(instance.path_file_root())
    except: pass
signals.pre_delete.connect(delete_download, sender=Download)

# -*- coding: utf-8 -*-
from django.contrib import admin
from models import Download

def start_torrent(modeladmin, request, queryset):
    for query in queryset: query.download()
start_torrent.short_description = u"Iniciar torrent"

class AdminDownload(admin.ModelAdmin):

    list_display = ("description", "status", "download_rate", "upload_rate", "_actions")
    search_fields = ("description", )
    list_filter = ("date",)
    save_on_top = True

    actions = [start_torrent, ]

    class Media:
        js = (
            'admin_tools/js/list_view_refresh.js',
        )

    fieldsets = [
        (u'Download',                   {'fields' : ("description", "magnetic", "torrent", ), }, ),
    ]


admin.site.register(Download, AdminDownload)
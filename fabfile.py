from __future__ import with_statement
from fabric.api import *

env.hosts = [
    'soda@neuroinfectologia.sodateste.com.br',
]
env.warn_only = True

def deploy():
    with cd('/deploy/sites/neuroinfectologia/neuroinfectologia'):
        # get lastest version from git
        run('git pull')
        # restart nginx
        run('../nginx/bin/restart')

